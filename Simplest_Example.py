import tensorflow as tf   #import the Tensorflow library
#to install tensorflow follow this link
#https://www.tensorflow.org/install/pip
 

sess = tf.Session() # Create a Tensorflow session to run the defined graphs (not needed when using eager executin) 

# The simplest example : A+B 
#Create two so called Tensors in the current graph
A = tf.constant(7.0)
B = tf.constant(6.0)
print(A)
print(B)
#add an mathematical operation to the graph. In this case just addition
C = tf.add(A,B)
print(C)
#At this point nothing is actualy calculated. We need to tell tensorflow that we want to execute the graph by running sess.run((C))
numerical_C = sess.run(C)
#We can actually add two numbers. Amazing.
print(numerical_C) 
