import tensorflow as tf   #import the Tensorflow library
import matplotlib.pyplot as plt   #Image display 
import numpy as np #standard numpy 


#This short handson tutorial describes how to train several neural networks with the tensorflow library on the MNIST dataset. 

#loading the data
# MNIST are 28x28 grayscale images of handwritten digits. 
mnist = tf.keras.datasets.mnist 
(x_train,y_train),(x_test,y_test) = mnist.load_data()
print(np.shape(x_train))
print(np.shape(y_train))
#---------------------------------------------------------------------------------------------------------------------
#First: linear regression!
#---------------------------------------------------------------------------------------------------------------------
sess = tf.Session() # Create a Tensorflow session to run the defined graphs (not needed when using eager executin) 


#A Tensorflow graph is made by Tensors and Variables. 
# Tensors are initialized with a value at the start of "sess.run()" while variables keep their values even for multiple calls of "sessrun()"

#--------------------
#creating the graph:
#creating a placeholder tensor for a single image. This will be used to put an image into the graph at runtime. 
#image_placeholder = tf.placeholder(tf.float32, (20,28*28)) #image placeholder is ofsize 28x28
image_placeholder = tf.placeholder(tf.float32, (20,28,28,1)) # placeholder for neural network 
output_placeholder = tf.placeholder(tf.int32, (20)) #output placeholder is an int from 0 to 9


#creating a Tensor of 28x28 values with the normal distribution. At this point this is nothing but a Tensor in a Graph. No actual values are created at this point
starting_values = tf.random_normal((28*28,10), mean = 0.0, stddev = 1.0)
#starting_values = tf.zeros((10,28*28))

# Variables are Tensors whose values can be adjusted by learning algorithms 
#mat = tf.Variable(starting_values, trainable=True, name='matrix',dtype=tf.float32)

#perform the only real mathematical operation of our model: image_placeholder * vec
#result = tf.matmul(image_placeholder,mat)
#print(result)
conv1 = tf.layers.conv2d(image_placeholder, 40, 5, activation = tf.nn.relu) #inp size ( 20,28,28,1) out size (20,28,28,40)
pooling1 = tf.layers.max_pooling2d(conv1, 2,2)				    #inp size ( 20,28,28,40) out size (20,28,28,40)
conv2 = tf.layers.conv2d(pooling1, 40, 5, activation = tf.nn.relu)	    #inp size ( 20,14,14,40) out size (20,14,14,40)	
pooling2 = tf.layers.max_pooling2d(conv2, 2,2)                              #inp size ( 20,14,14,40) out size (20,7,7,40)
			

flat = tf.layers.flatten(pooling2)					    #inp size (20,7,7,40) out size (20,7*7*40)	
dense1 = tf.layers.dense(flat, 100)					    #inp size (20,7*7*40) out size (20,100)
result = tf.layers.dense(dense1, 10)					    #inp size (20,100) out size (20,10)

#------------
#calculate the loss: In this case we will use mean squared error. (There are functions for this when in actual use but its more understandable this way) 
output_one_hot = tf.one_hot(output_placeholder, 10)
#calculate the element wise product of diff, summing it up and taking the square root of the result. This makes a good loss (in short, calculate the mean squared error) 
mse = tf.losses.mean_squared_error(output_one_hot,result)

#We now need to tell Tensorflow to optimize for a minimum loss. Luckily there are functions build in Tensorflow to perform variations of gradient descent. We will use simple gradient descent to see how it holds up. Learning rate is fixed at 0.01
gradient_descent_optimizer = tf.train.GradientDescentOptimizer(0.1)
#gradient_descent_optimizer = tf.train.AdamOptimizer()
#We now need to tell the optimizer which value it is supposed to optimize. Everything else will be done by the graph structure we defined above. This will return a "learning operation" which we can run with sess.run((learning_operation))
train_operation = gradient_descent_optimizer.minimize(mse)

# The last thing we need is an operation to initialize our matrix with random values. This needs to be done only once. 
init = tf.global_variables_initializer() #gets ALL the variables in the current graph, checks how to initialize them and returns an operation to do so
#Now the first actual interaction with the Tensorflow graph is run. All we did above was just creating an elaborate discription of our calculation graph.    
sess.run(init) # this runs the variables initializer 

#-------------------
#we can now perform the training
for epoch in range(0,2):
    for i in range(0,int(np.shape(x_train)[0]/20)):
        #create an input for our graph
        image = np.reshape(x_train[i*20:(i+1)*20], (20,28*28)) /255
        output = np.reshape(y_train[i*20:(i+1)*20], (20))
        image_conv = np.reshape(x_train[i*20:(i+1)*20], (20,28,28,1))/255

        
        
        #Run the graph we defined above. The first option tells Tensorflow for which Tensors we want numerical values. The feed_dict collection sets the input of our graph
        #Training is also done in this operation because we tell Tensorflow to perform the training operation.
        _, numerical_loss= sess.run((train_operation, mse), feed_dict = {image_placeholder:image_conv, output_placeholder:output}) 
        #print(numerical_loss)

#----------------------
#Test the result:
count = 0
for i in range(0,int(np.shape(x_test)[0]/20)):
    image = np.reshape(x_train[i*20:(i+1)*20], (20,28*28))/255
    output = np.reshape(y_train[i*20:(i+1)*20], (20))
    image_conv = np.reshape(x_train[i*20:(i+1)*20], (20,28,28,1))/255
    numerical_result,num_output_one_hot = sess.run((result,output_one_hot), feed_dict={image_placeholder:image_conv, output_placeholder:output})
    
    max_num_res = np.zeros((20,10))
    for j in range(0,20):
        max_num_res[j,:] = numerical_result[j,:] == np.max(numerical_result[j,:])
        #max_num_res = numerical_result == np.max(numerical_result)
    max_num_res = max_num_res.astype(float)
    
    #max_num_res = np.reshape(max_num_res,(10,20)).astype(float)
    #num_output_one_hot = np.reshape(num_output_one_hot,(10,20))
    #print(max_num_res)
    #print(num_output_one_hot)
    #input()
    for i in range(0,20):
        if np.array_equal(max_num_res[i,:],num_output_one_hot[i,:]):
         count = count +1

print("{}/{} {}%".format(count, np.shape(x_test)[0], (count/np.shape(x_test)[0])*100))

    







